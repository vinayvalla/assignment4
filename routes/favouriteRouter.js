const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var authenticate = require('../authenticate');
const Favorites = require('../models/favorite');

const favRouter=express.Router();

favRouter.use(bodyParser.json());

favRouter.route('/')
.get(authenticate.verifyUser,(req,res,next)=>{
Favorites.find({}).populate('user').populate('dishes').then((favs)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(favs);
}, (err) => next(err))
.catch((err) => next(err));
})
    .post(authenticate.verifyUser, (req, res, next) => {

        Favorites.findOne({ 'user': req.user._id }).then((fav) => {
            console.log(fav);
            if (!fav) {

                Favorites.create({ user: req.user._id, dishes: req.body })
                    .then((fav) => {
                        res.setHeader('Content-Type', 'application/json');
                        res.json(fav);
                    }, (err) => next(err))
            }
            else {
                var s=req.body;
               for (let i=0;i<s.length;i++){
                 var dish = s[i]._id;
                if (fav.dishes.indexOf(dish) == -1) {
                    fav.dishes.push(dish);
                }     
            }    
            fav.save(function (err, favorite) {
                if (err) throw err;
                res.json(favorite);
            });    
            }
        }
            , (err) => next(err))

            .catch((err) => next(err));
    });

favRouter.route('/:dishId')
.get(authenticate.verifyUser,(req,res,next)=>{
    Favorites.findById(req.params.dishId).populate('user').populate('dishes').then((favs)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(favs);
    }, (err) => next(err))
    .catch((err) => next(err));
    })
    .post(authenticate.verifyUser, (req, res, next) => {
        //    console.log(req.user);
        
        Favorites.findOne({'user' : req.user._id}).then((fav) => {
            //console.log(fav);
            if (!fav) {

                //adding user
                Favorites.create({user: req.user._id , dishes : req.params.dishId})
                    .then((fav) => {
                      
                            res.setHeader('Content-Type', 'application/json');
                            res.json(fav);
                        

                    }, (err) => next(err))


            }
            else {
               if (fav.dishes.indexOf(req.params.dishId) == -1)
                {
                fav.dishes.push({"_id":req.params.dishId});
                }
                fav.save()
                .then((fav) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(fav);                
                }, (err) => next(err));
            }  
            

        }, (err) => next(err))
            .catch((err) => next(err));
    })
    .delete(authenticate.verifyUser,(req,res,next)=>{
        Favorites.findOne({'user' : req.user._id}).populate('user').populate('dishes').then((fav) => {
            if (fav.dishes.indexOf(req.params.dishId) != -1)
                {
                    fav.dishes.id(req.params.dishId).remove();
                }
                fav.save()
                .then((fav) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(fav);                
                }, (err) => next(err));
        }, (err) => next(err));
    });
    
module.exports = favRouter;